module gitlab.com/dhendel/terraform-provider-awx

go 1.12

require (
	github.com/hashicorp/terraform v0.12.6
	gitlab.com/dhendel/awx-go v0.0.10-0.20190819154037-56b56be0cb9c
	gopkg.in/yaml.v2 v2.2.2
)
